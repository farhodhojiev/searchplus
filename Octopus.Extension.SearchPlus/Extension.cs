﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Octopus.Extensions;

namespace Octopus.Extension.SearchPlus
{
    [Export(typeof(IExtension))]
    public class Extension : IExtension
    {
        #region IExtension Members

        public string GetMeta(string key)
        {
            var meta = new Dictionary<string, string>
            {
                {"Name", "SearchPlus Extension"},
                {"OctopusVersion", "v4.12.0"},
                {"Vendor", "OTJ Solutions FHR"},
                {"Version", "v2.0.0"}
            };
            return meta.ContainsKey(key) ? meta[key] : string.Empty;
        }

        public Guid Guid
        {
            get { return new Guid("472FB553-C38D-4d6b-832B-8A77C0CD574C"); }
        }

        public object QueryInterface(Type t)
        {
            if (null == t) return null;
            var map = new Dictionary<Type, Type>
            {
                {typeof(IMenu), typeof(Menu)}
            };

            var targetType = map.ContainsKey(t) ? map[t] : null;
            if (null == targetType) return null;
            var ctor = targetType.GetConstructor(new Type[] { });
            return null == ctor ? null : ctor.Invoke(new object[] { });
        }

        #endregion
    }
}
