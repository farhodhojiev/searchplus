﻿namespace Octopus.Extension.SearchPlus
{
    partial class InfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfoForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_dateofbirth = new System.Windows.Forms.Label();
            this.label_passport = new System.Windows.Forms.Label();
            this.label_district = new System.Windows.Forms.Label();
            this.label_city = new System.Windows.Forms.Label();
            this.label_address = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label_inn = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_clientname = new System.Windows.Forms.LinkLabel();
            this.link_guarator_for = new System.Windows.Forms.LinkLabel();
            this.listView_guarator_for = new System.Windows.Forms.ListView();
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel_guarantor_for = new System.Windows.Forms.Panel();
            this.link_member_of = new System.Windows.Forms.LinkLabel();
            this.listView_member_of = new System.Windows.Forms.ListView();
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader59 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel_member_of = new System.Windows.Forms.Panel();
            this.panel_solidary_group_loan = new System.Windows.Forms.Panel();
            this.listView_solidary_group_loan = new System.Windows.Forms.ListView();
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.link_solidary_group_loan = new System.Windows.Forms.LinkLabel();
            this.panel_collateral_owner = new System.Windows.Forms.Panel();
            this.listView_collateral_owner = new System.Windows.Forms.ListView();
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.link_collateral_owner = new System.Windows.Forms.LinkLabel();
            this.link_loans = new System.Windows.Forms.LinkLabel();
            this.panel_loans = new System.Windows.Forms.Panel();
            this.listView_loans = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader58 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader57 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.link_savings = new System.Windows.Forms.LinkLabel();
            this.panel_savings = new System.Windows.Forms.Panel();
            this.listView_savings = new System.Windows.Forms.ListView();
            this.columnHeader47 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader49 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader50 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader51 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader52 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader53 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader54 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader55 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader56 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.panel_guarantor_for.SuspendLayout();
            this.panel_member_of.SuspendLayout();
            this.panel_solidary_group_loan.SuspendLayout();
            this.panel_collateral_owner.SuspendLayout();
            this.panel_loans.SuspendLayout();
            this.panel_savings.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(16, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Client name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(14, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date of birth:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(37, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Passport:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(348, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "District:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(366, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "City:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(341, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Address:";
            // 
            // label_dateofbirth
            // 
            this.label_dateofbirth.AutoSize = true;
            this.label_dateofbirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_dateofbirth.Location = new System.Drawing.Point(110, 23);
            this.label_dateofbirth.Name = "label_dateofbirth";
            this.label_dateofbirth.Size = new System.Drawing.Size(76, 15);
            this.label_dateofbirth.TabIndex = 7;
            this.label_dateofbirth.Text = "Date of birth:";
            // 
            // label_passport
            // 
            this.label_passport.AutoSize = true;
            this.label_passport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_passport.Location = new System.Drawing.Point(110, 41);
            this.label_passport.Name = "label_passport";
            this.label_passport.Size = new System.Drawing.Size(58, 15);
            this.label_passport.TabIndex = 8;
            this.label_passport.Text = "Passport:";
            // 
            // label_district
            // 
            this.label_district.AutoSize = true;
            this.label_district.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_district.Location = new System.Drawing.Point(410, 5);
            this.label_district.Name = "label_district";
            this.label_district.Size = new System.Drawing.Size(47, 15);
            this.label_district.TabIndex = 9;
            this.label_district.Text = "District:";
            // 
            // label_city
            // 
            this.label_city.AutoSize = true;
            this.label_city.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_city.Location = new System.Drawing.Point(410, 23);
            this.label_city.Name = "label_city";
            this.label_city.Size = new System.Drawing.Size(29, 15);
            this.label_city.TabIndex = 10;
            this.label_city.Text = "City:";
            // 
            // label_address
            // 
            this.label_address.AutoSize = true;
            this.label_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_address.Location = new System.Drawing.Point(410, 41);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(54, 15);
            this.label_address.TabIndex = 11;
            this.label_address.Text = "Address:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(726, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "INN:";
            // 
            // label_inn
            // 
            this.label_inn.AutoSize = true;
            this.label_inn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_inn.Location = new System.Drawing.Point(769, 41);
            this.label_inn.Name = "label_inn";
            this.label_inn.Size = new System.Drawing.Size(31, 15);
            this.label_inn.TabIndex = 18;
            this.label_inn.Text = "INN:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label_clientname);
            this.panel1.Controls.Add(this.label_passport);
            this.panel1.Controls.Add(this.label_inn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label_dateofbirth);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label_address);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label_city);
            this.panel1.Controls.Add(this.label_district);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 67);
            this.panel1.TabIndex = 19;
            // 
            // label_clientname
            // 
            this.label_clientname.AutoSize = true;
            this.label_clientname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_clientname.Location = new System.Drawing.Point(110, 5);
            this.label_clientname.Name = "label_clientname";
            this.label_clientname.Size = new System.Drawing.Size(73, 15);
            this.label_clientname.TabIndex = 19;
            this.label_clientname.TabStop = true;
            this.label_clientname.Text = "Client name";
            this.label_clientname.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.label_clientname_LinkClicked);
            // 
            // link_guarator_for
            // 
            this.link_guarator_for.AutoSize = true;
            this.link_guarator_for.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_guarator_for.Location = new System.Drawing.Point(0, 341);
            this.link_guarator_for.Name = "link_guarator_for";
            this.link_guarator_for.Padding = new System.Windows.Forms.Padding(10, 7, 10, 2);
            this.link_guarator_for.Size = new System.Drawing.Size(98, 22);
            this.link_guarator_for.TabIndex = 20;
            this.link_guarator_for.TabStop = true;
            this.link_guarator_for.Text = "+ Guarantor for";
            this.link_guarator_for.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_guarator_for_LinkClicked);
            // 
            // listView_guarator_for
            // 
            this.listView_guarator_for.AllowColumnReorder = true;
            this.listView_guarator_for.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader17,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.listView_guarator_for.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_guarator_for.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_guarator_for.FullRowSelect = true;
            this.listView_guarator_for.GridLines = true;
            this.listView_guarator_for.LabelWrap = false;
            this.listView_guarator_for.Location = new System.Drawing.Point(0, 0);
            this.listView_guarator_for.MultiSelect = false;
            this.listView_guarator_for.Name = "listView_guarator_for";
            this.listView_guarator_for.Size = new System.Drawing.Size(955, 107);
            this.listView_guarator_for.TabIndex = 0;
            this.listView_guarator_for.UseCompatibleStateImageBehavior = false;
            this.listView_guarator_for.View = System.Windows.Forms.View.Details;
            this.listView_guarator_for.DoubleClick += new System.EventHandler(this.listView_guarator_for_DoubleClick);
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "contract_id";
            this.columnHeader17.Width = 4;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Clent Name";
            this.columnHeader10.Width = 163;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Contract code";
            this.columnHeader11.Width = 132;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Status";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 61;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Amount";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader13.Width = 94;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Start date";
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader14.Width = 79;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "End date";
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader15.Width = 81;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Guaranteed amount";
            this.columnHeader16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader16.Width = 89;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "% of Loan";
            this.columnHeader18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "OLB";
            this.columnHeader19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Description";
            this.columnHeader20.Width = 93;
            // 
            // panel_guarantor_for
            // 
            this.panel_guarantor_for.Controls.Add(this.listView_guarator_for);
            this.panel_guarantor_for.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_guarantor_for.Location = new System.Drawing.Point(0, 363);
            this.panel_guarantor_for.Name = "panel_guarantor_for";
            this.panel_guarantor_for.Size = new System.Drawing.Size(955, 107);
            this.panel_guarantor_for.TabIndex = 21;
            // 
            // link_member_of
            // 
            this.link_member_of.AutoSize = true;
            this.link_member_of.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_member_of.Location = new System.Drawing.Point(0, 470);
            this.link_member_of.Name = "link_member_of";
            this.link_member_of.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_member_of.Size = new System.Drawing.Size(86, 20);
            this.link_member_of.TabIndex = 22;
            this.link_member_of.TabStop = true;
            this.link_member_of.Text = "+ Member of";
            this.link_member_of.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_member_of_LinkClicked);
            // 
            // listView_member_of
            // 
            this.listView_member_of.AllowColumnReorder = true;
            this.listView_member_of.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader59,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26});
            this.listView_member_of.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_member_of.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_member_of.FullRowSelect = true;
            this.listView_member_of.GridLines = true;
            this.listView_member_of.LabelWrap = false;
            this.listView_member_of.Location = new System.Drawing.Point(0, 0);
            this.listView_member_of.MultiSelect = false;
            this.listView_member_of.Name = "listView_member_of";
            this.listView_member_of.Size = new System.Drawing.Size(955, 112);
            this.listView_member_of.TabIndex = 0;
            this.listView_member_of.UseCompatibleStateImageBehavior = false;
            this.listView_member_of.View = System.Windows.Forms.View.Details;
            this.listView_member_of.DoubleClick += new System.EventHandler(this.listView_member_of_DoubleClick);
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "group_id";
            this.columnHeader21.Width = 4;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Name";
            this.columnHeader22.Width = 197;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Type of Group";
            this.columnHeader23.Width = 120;
            // 
            // columnHeader59
            // 
            this.columnHeader59.Text = "Active";
            this.columnHeader59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader59.Width = 64;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Establishment date";
            this.columnHeader24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader24.Width = 159;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Joined date";
            this.columnHeader25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader25.Width = 168;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Left date";
            this.columnHeader26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader26.Width = 162;
            // 
            // panel_member_of
            // 
            this.panel_member_of.Controls.Add(this.listView_member_of);
            this.panel_member_of.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_member_of.Location = new System.Drawing.Point(0, 490);
            this.panel_member_of.Name = "panel_member_of";
            this.panel_member_of.Size = new System.Drawing.Size(955, 112);
            this.panel_member_of.TabIndex = 23;
            // 
            // panel_solidary_group_loan
            // 
            this.panel_solidary_group_loan.Controls.Add(this.listView_solidary_group_loan);
            this.panel_solidary_group_loan.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_solidary_group_loan.Location = new System.Drawing.Point(0, 622);
            this.panel_solidary_group_loan.Name = "panel_solidary_group_loan";
            this.panel_solidary_group_loan.Size = new System.Drawing.Size(955, 116);
            this.panel_solidary_group_loan.TabIndex = 25;
            // 
            // listView_solidary_group_loan
            // 
            this.listView_solidary_group_loan.AllowColumnReorder = true;
            this.listView_solidary_group_loan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35});
            this.listView_solidary_group_loan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_solidary_group_loan.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_solidary_group_loan.FullRowSelect = true;
            this.listView_solidary_group_loan.GridLines = true;
            this.listView_solidary_group_loan.LabelWrap = false;
            this.listView_solidary_group_loan.Location = new System.Drawing.Point(0, 0);
            this.listView_solidary_group_loan.MultiSelect = false;
            this.listView_solidary_group_loan.Name = "listView_solidary_group_loan";
            this.listView_solidary_group_loan.Size = new System.Drawing.Size(955, 116);
            this.listView_solidary_group_loan.TabIndex = 0;
            this.listView_solidary_group_loan.UseCompatibleStateImageBehavior = false;
            this.listView_solidary_group_loan.View = System.Windows.Forms.View.Details;
            this.listView_solidary_group_loan.DoubleClick += new System.EventHandler(this.listView_solidary_group_loan_DoubleClick);
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "contract_id";
            this.columnHeader27.Width = 7;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Group Name";
            this.columnHeader28.Width = 145;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Contract code";
            this.columnHeader29.Width = 164;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Status";
            this.columnHeader30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader30.Width = 78;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Amount";
            this.columnHeader31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Share amount";
            this.columnHeader32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader32.Width = 106;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Start date";
            this.columnHeader33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader33.Width = 68;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "End date";
            this.columnHeader34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader34.Width = 68;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Loan officer";
            this.columnHeader35.Width = 76;
            // 
            // link_solidary_group_loan
            // 
            this.link_solidary_group_loan.AutoSize = true;
            this.link_solidary_group_loan.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_solidary_group_loan.Location = new System.Drawing.Point(0, 602);
            this.link_solidary_group_loan.Name = "link_solidary_group_loan";
            this.link_solidary_group_loan.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_solidary_group_loan.Size = new System.Drawing.Size(126, 20);
            this.link_solidary_group_loan.TabIndex = 24;
            this.link_solidary_group_loan.TabStop = true;
            this.link_solidary_group_loan.Text = "+ Solidary group loan";
            this.link_solidary_group_loan.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_solidary_group_loan_LinkClicked);
            // 
            // panel_collateral_owner
            // 
            this.panel_collateral_owner.Controls.Add(this.listView_collateral_owner);
            this.panel_collateral_owner.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_collateral_owner.Location = new System.Drawing.Point(0, 758);
            this.panel_collateral_owner.Name = "panel_collateral_owner";
            this.panel_collateral_owner.Size = new System.Drawing.Size(955, 119);
            this.panel_collateral_owner.TabIndex = 27;
            // 
            // listView_collateral_owner
            // 
            this.listView_collateral_owner.AllowColumnReorder = true;
            this.listView_collateral_owner.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader40,
            this.columnHeader41,
            this.columnHeader42,
            this.columnHeader43,
            this.columnHeader44});
            this.listView_collateral_owner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_collateral_owner.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_collateral_owner.FullRowSelect = true;
            this.listView_collateral_owner.GridLines = true;
            this.listView_collateral_owner.LabelWrap = false;
            this.listView_collateral_owner.Location = new System.Drawing.Point(0, 0);
            this.listView_collateral_owner.MultiSelect = false;
            this.listView_collateral_owner.Name = "listView_collateral_owner";
            this.listView_collateral_owner.Size = new System.Drawing.Size(955, 119);
            this.listView_collateral_owner.TabIndex = 0;
            this.listView_collateral_owner.UseCompatibleStateImageBehavior = false;
            this.listView_collateral_owner.View = System.Windows.Forms.View.Details;
            this.listView_collateral_owner.DoubleClick += new System.EventHandler(this.listView_collateral_owner_DoubleClick);
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "contract_id";
            this.columnHeader36.Width = 5;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Clent Name";
            this.columnHeader37.Width = 145;
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Contract code";
            this.columnHeader38.Width = 164;
            // 
            // columnHeader39
            // 
            this.columnHeader39.Text = "Status";
            this.columnHeader39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader39.Width = 78;
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Amount";
            this.columnHeader40.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Start date";
            this.columnHeader41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "End date";
            this.columnHeader42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader42.Width = 69;
            // 
            // columnHeader43
            // 
            this.columnHeader43.Text = "Loan officer";
            this.columnHeader43.Width = 84;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Text = "Collateral type";
            this.columnHeader44.Width = 83;
            // 
            // link_collateral_owner
            // 
            this.link_collateral_owner.AutoSize = true;
            this.link_collateral_owner.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_collateral_owner.Location = new System.Drawing.Point(0, 738);
            this.link_collateral_owner.Name = "link_collateral_owner";
            this.link_collateral_owner.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_collateral_owner.Size = new System.Drawing.Size(111, 20);
            this.link_collateral_owner.TabIndex = 26;
            this.link_collateral_owner.TabStop = true;
            this.link_collateral_owner.Text = "+ Collateral owner";
            this.link_collateral_owner.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_collateral_owner_LinkClicked);
            // 
            // link_loans
            // 
            this.link_loans.AutoSize = true;
            this.link_loans.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_loans.Location = new System.Drawing.Point(0, 67);
            this.link_loans.Name = "link_loans";
            this.link_loans.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_loans.Size = new System.Drawing.Size(65, 20);
            this.link_loans.TabIndex = 28;
            this.link_loans.TabStop = true;
            this.link_loans.Text = "+ Loans";
            this.link_loans.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_loans_LinkClicked);
            // 
            // panel_loans
            // 
            this.panel_loans.Controls.Add(this.listView_loans);
            this.panel_loans.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_loans.Location = new System.Drawing.Point(0, 87);
            this.panel_loans.Name = "panel_loans";
            this.panel_loans.Size = new System.Drawing.Size(955, 115);
            this.panel_loans.TabIndex = 29;
            // 
            // listView_loans
            // 
            this.listView_loans.AllowColumnReorder = true;
            this.listView_loans.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader45,
            this.columnHeader46,
            this.columnHeader58,
            this.columnHeader57});
            this.listView_loans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_loans.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_loans.FullRowSelect = true;
            this.listView_loans.GridLines = true;
            this.listView_loans.LabelWrap = false;
            this.listView_loans.Location = new System.Drawing.Point(0, 0);
            this.listView_loans.MultiSelect = false;
            this.listView_loans.Name = "listView_loans";
            this.listView_loans.Size = new System.Drawing.Size(955, 115);
            this.listView_loans.TabIndex = 1;
            this.listView_loans.UseCompatibleStateImageBehavior = false;
            this.listView_loans.View = System.Windows.Forms.View.Details;
            this.listView_loans.SelectedIndexChanged += new System.EventHandler(this.listView_loans_SelectedIndexChanged);
            this.listView_loans.DoubleClick += new System.EventHandler(this.listView_loans_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "contract_id";
            this.columnHeader1.Width = 6;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Code";
            this.columnHeader2.Width = 163;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 54;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Amount";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 61;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "OLB";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 68;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Currency";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 54;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Interest rate";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 69;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Installment type";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 89;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "N of installments";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader9.Width = 57;
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "Creation date";
            this.columnHeader45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader45.Width = 76;
            // 
            // columnHeader46
            // 
            this.columnHeader46.Text = "Start date";
            this.columnHeader46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader46.Width = 67;
            // 
            // columnHeader58
            // 
            this.columnHeader58.Text = "End date";
            this.columnHeader58.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader58.Width = 77;
            // 
            // columnHeader57
            // 
            this.columnHeader57.Text = "Loan officer";
            this.columnHeader57.Width = 70;
            // 
            // link_savings
            // 
            this.link_savings.AutoSize = true;
            this.link_savings.Dock = System.Windows.Forms.DockStyle.Top;
            this.link_savings.Location = new System.Drawing.Point(0, 202);
            this.link_savings.Name = "link_savings";
            this.link_savings.Padding = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.link_savings.Size = new System.Drawing.Size(74, 20);
            this.link_savings.TabIndex = 30;
            this.link_savings.TabStop = true;
            this.link_savings.Text = "+ Savings";
            this.link_savings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_savings_LinkClicked);
            // 
            // panel_savings
            // 
            this.panel_savings.Controls.Add(this.listView_savings);
            this.panel_savings.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_savings.Location = new System.Drawing.Point(0, 222);
            this.panel_savings.Name = "panel_savings";
            this.panel_savings.Size = new System.Drawing.Size(955, 119);
            this.panel_savings.TabIndex = 31;
            // 
            // listView_savings
            // 
            this.listView_savings.AllowColumnReorder = true;
            this.listView_savings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader47,
            this.columnHeader48,
            this.columnHeader49,
            this.columnHeader50,
            this.columnHeader51,
            this.columnHeader52,
            this.columnHeader53,
            this.columnHeader54,
            this.columnHeader55,
            this.columnHeader56});
            this.listView_savings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_savings.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_savings.FullRowSelect = true;
            this.listView_savings.GridLines = true;
            this.listView_savings.LabelWrap = false;
            this.listView_savings.Location = new System.Drawing.Point(0, 0);
            this.listView_savings.MultiSelect = false;
            this.listView_savings.Name = "listView_savings";
            this.listView_savings.Size = new System.Drawing.Size(955, 119);
            this.listView_savings.TabIndex = 1;
            this.listView_savings.UseCompatibleStateImageBehavior = false;
            this.listView_savings.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader47
            // 
            this.columnHeader47.Text = "contract_id";
            this.columnHeader47.Width = 4;
            // 
            // columnHeader48
            // 
            this.columnHeader48.Text = "Code";
            this.columnHeader48.Width = 163;
            // 
            // columnHeader49
            // 
            this.columnHeader49.Text = "Type";
            this.columnHeader49.Width = 84;
            // 
            // columnHeader50
            // 
            this.columnHeader50.Text = "Description";
            this.columnHeader50.Width = 70;
            // 
            // columnHeader51
            // 
            this.columnHeader51.Text = "Balance";
            this.columnHeader51.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader51.Width = 94;
            // 
            // columnHeader52
            // 
            this.columnHeader52.Text = "Currency";
            this.columnHeader52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader52.Width = 79;
            // 
            // columnHeader53
            // 
            this.columnHeader53.Text = "Creation date";
            this.columnHeader53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader53.Width = 81;
            // 
            // columnHeader54
            // 
            this.columnHeader54.Text = "Last deposit/withdraw date";
            this.columnHeader54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader54.Width = 146;
            // 
            // columnHeader55
            // 
            this.columnHeader55.Text = "Status";
            this.columnHeader55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader56
            // 
            this.columnHeader56.Text = "Close date";
            this.columnHeader56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader56.Width = 80;
            // 
            // InfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(972, 482);
            this.Controls.Add(this.panel_collateral_owner);
            this.Controls.Add(this.link_collateral_owner);
            this.Controls.Add(this.panel_solidary_group_loan);
            this.Controls.Add(this.link_solidary_group_loan);
            this.Controls.Add(this.panel_member_of);
            this.Controls.Add(this.link_member_of);
            this.Controls.Add(this.panel_guarantor_for);
            this.Controls.Add(this.link_guarator_for);
            this.Controls.Add(this.panel_savings);
            this.Controls.Add(this.link_savings);
            this.Controls.Add(this.panel_loans);
            this.Controls.Add(this.link_loans);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InfoForm";
            this.Text = "Client Info";
            this.Load += new System.EventHandler(this.InfoForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_guarantor_for.ResumeLayout(false);
            this.panel_member_of.ResumeLayout(false);
            this.panel_solidary_group_loan.ResumeLayout(false);
            this.panel_collateral_owner.ResumeLayout(false);
            this.panel_loans.ResumeLayout(false);
            this.panel_savings.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_dateofbirth;
        private System.Windows.Forms.Label label_passport;
        private System.Windows.Forms.Label label_district;
        private System.Windows.Forms.Label label_city;
        private System.Windows.Forms.Label label_address;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label_inn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel link_guarator_for;
        public System.Windows.Forms.ListView listView_guarator_for;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.Panel panel_guarantor_for;
        private System.Windows.Forms.LinkLabel link_member_of;
        public System.Windows.Forms.ListView listView_member_of;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Panel panel_member_of;
        private System.Windows.Forms.Panel panel_solidary_group_loan;
        public System.Windows.Forms.ListView listView_solidary_group_loan;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.LinkLabel link_solidary_group_loan;
        private System.Windows.Forms.Panel panel_collateral_owner;
        public System.Windows.Forms.ListView listView_collateral_owner;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.LinkLabel link_collateral_owner;
        private System.Windows.Forms.LinkLabel link_loans;
        private System.Windows.Forms.Panel panel_loans;
        private System.Windows.Forms.LinkLabel link_savings;
        private System.Windows.Forms.Panel panel_savings;
        public System.Windows.Forms.ListView listView_loans;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        public System.Windows.Forms.ListView listView_savings;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.LinkLabel label_clientname;
        private System.Windows.Forms.ColumnHeader columnHeader59;
    }
}