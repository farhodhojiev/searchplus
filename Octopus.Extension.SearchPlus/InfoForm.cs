﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Octopus.CoreDomain;
using Octopus.CoreDomain.Clients;
using Octopus.CoreDomain.Contracts.Loans;
using Octopus.GUI.Clients;
using Octopus.GUI;
using Octopus.Services;
using Octopus.Enums;

namespace Octopus.Extension.SearchPlus
{
    public partial class InfoForm : Form
    {
        public SearchForm SearchForm_v { get; set; } //connect with main form for call method

        public InfoForm()
        {
            InitializeComponent();
        }

        string lmax_loans = "- Loans";
        string lmin_loans = "+ Loans";
        string lmax_savings = "- Savings";
        string lmin_savings = "+ Savings";
        string lmax_guarantor_for = "- Guarantor for";
        string lmin_guarantor_for = "+ Guarantor for";
        string lmax_member_of = "- Member of";
        string lmin_member_of = "+ Member of";
        string lmax_solidary_group_loan = "- Solidary group loan";
        string lmin_solidary_group_loan = "+ Solidary group loan";
        string lmax_collateral_owner = "- Collateral owner";
        string lmin_collateral_owner = "+ Collateral owner";

        int thisDatabase = 0;

        private void InfoForm_Load(object sender, EventArgs e)
        {
            //////////////////////////////////////////////////// Check database, this ir not
            string query = "SELECT DB_NAME() AS DataBaseName";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    String DataBaseName=reader.GetString(reader.GetOrdinal("DataBaseName"));
                    if (DataBaseName == SearchForm_v.list_selected_dbname)
                    {
                        thisDatabase = 1;
                    }
                }
            }
            //////////////////////////////////////////////////// load client info
            query = @"EXEC ('USE ' + @database_select + '
                    SELECT
                    p.first_name+SPACE(1)+p.last_name+SPACE(1)+ISNULL(p.father_name,'''') AS FIO,
                    CONVERT(NVARCHAR(10),p.birth_date, 120) AS birth_date,
                    p.identification_data AS passport,
                    d.name AS district,
                    ISNULL(t.city, '''') AS city,
                    ISNULL(t.[address], '''') AS cl_address,
                    
                    ISNULL(cfv.value, '''') AS personal_INN
                    FROM dbo.Persons p
                    LEFT JOIN dbo.Tiers t ON t.id=p.id
                    LEFT JOIN dbo.Districts d ON t.district_id=d.id
                    LEFT JOIN dbo.CustomFields cf ON cf.guid=''INN''
		            LEFT JOIN dbo.CustomFieldsValues cfv ON cfv.owner_id=p.id AND cfv.field_id=cf.id
                    WHERE p.id=' + @client_id + '')";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", SearchForm_v.list_selected_pid);
                cmd.Parameters.AddWithValue("@database_select", SearchForm_v.list_selected_dbname);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    label_clientname.Text = reader.GetString(reader.GetOrdinal("FIO"));
                    label_dateofbirth.Text = reader.GetString(reader.GetOrdinal("birth_date"));
                    label_passport.Text = reader.GetString(reader.GetOrdinal("passport"));
                    label_district.Text = reader.GetString(reader.GetOrdinal("district"));
                    label_city.Text = reader.GetString(reader.GetOrdinal("city"));
                    label_address.Text = reader.GetString(reader.GetOrdinal("cl_address"));

                    label_inn.Text = reader.GetString(reader.GetOrdinal("personal_INN"));
                }
            }

            //////////////////////////////////////////////////// load client history
            //SELECT Loans -------------------------------------
            //-------------------------------------------------------------------------
            query = @"EXEC ('USE ' + @database_select + '
                SELECT c.id AS id,
                c.contract_code AS contract_code,
                s.status_name AS status_name,
                cr.amount AS amount,
                dbo.GetOLB(c.id, GETDATE()) AS OLB,
                cu.name AS currency,
                CAST(cr.interest_rate*100 AS DECIMAL(10, 4)) AS interest_rate,
                it.name AS ins_type,
                cr.nb_of_installment AS num_installment,
                CONVERT(NVARCHAR(10), c.creation_date, 120) AS creation_date,
                CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
                CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
                u.last_name+SPACE(1)+u.first_name AS LO
                FROM dbo.Contracts c
                LEFT JOIN dbo.Credit cr ON cr.id=c.id
                LEFT JOIN dbo.Statuses s ON s.id=c.status
                LEFT JOIN dbo.Users u ON u.id=cr.loanofficer_id
                LEFT JOIN dbo.InstallmentTypes it ON it.id=cr.installment_type
                LEFT JOIN dbo.Packages pk ON pk.id=cr.package_id
                LEFT JOIN dbo.Currencies cu ON cu.id=pk.currency_id
                LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
                WHERE t.id='+@client_id+'')";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", SearchForm_v.list_selected_pid);
                cmd.Parameters.AddWithValue("@database_select", SearchForm_v.list_selected_dbname);
                var reader = cmd.ExecuteReader();

                variables.i_loans = 0; //set 0, because global
                if (reader.HasRows) { variables.i_loans = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    Decimal OLB = reader.GetDecimal(reader.GetOrdinal("OLB"));
                    Decimal interest_rate = reader.GetDecimal(reader.GetOrdinal("interest_rate"));
                    Decimal num_installment = reader.GetInt32(reader.GetOrdinal("num_installment"));
                    
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            OLB.ToString(),
                            reader.GetString(reader.GetOrdinal("currency")),
                            interest_rate.ToString(),
                            reader.GetString(reader.GetOrdinal("ins_type")),
                            num_installment.ToString(),
                            reader.GetString(reader.GetOrdinal("creation_date")),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            reader.GetString(reader.GetOrdinal("LO")),
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(listView_loans.Font, FontStyle.Bold);
                    }
                    listView_loans.Items.Add(listViewItemss);
                }
            }
            //SELECT guarantor_for -------------------------------------
            //-------------------------------------------------------------------------
            query = @"EXEC ('USE ' + @database_select + '
            SELECT
            c.id AS id,
            ISNULL(g.name, p.first_name + SPACE(1) + p.last_name+ SPACE(1)+ISNULL(p.father_name, '''')) AS [client_name],
            c.contract_code AS contract_code,
            s.status_name AS status_name,
            cr.amount AS amount,
            CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
            CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
            lgc.guarantee_amount AS guarantee_amount,
            lgc.guarantee_amount/cr.amount*100 AS procent_of_cr,
            dbo.GetOLB(c.id, GETDATE()) AS OLB,
            ISNULL(lgc.guarantee_desc, ''-'') AS description
            FROM dbo.Contracts c
            LEFT JOIN dbo.Credit cr ON cr.id=c.id
            LEFT JOIN dbo.Statuses s ON s.id=c.status
            LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
            LEFT JOIN dbo.Persons p ON p.id=t.id
            LEFT JOIN dbo.Groups g ON g.id=t.id
            LEFT JOIN dbo.LinkGuarantorCredit lgc ON lgc.contract_id=c.id
            WHERE lgc.tiers_id='+@client_id+'')";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", SearchForm_v.list_selected_pid);
                cmd.Parameters.AddWithValue("@database_select", SearchForm_v.list_selected_dbname);
                var reader = cmd.ExecuteReader();

                variables.i_guarantor_for = 0; //set 0, because global
                if (reader.HasRows) { variables.i_guarantor_for = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    Decimal guarantee_amount = reader.GetDecimal(reader.GetOrdinal("guarantee_amount"));
                    Decimal procent_of_cr = reader.GetDecimal(reader.GetOrdinal("procent_of_cr"));
                    Decimal OLB = reader.GetDecimal(reader.GetOrdinal("OLB"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            guarantee_amount.ToString(),
                            procent_of_cr.ToString(),
                            OLB.ToString(),
                            reader.GetString(reader.GetOrdinal("description")),
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(listView_guarator_for.Font, FontStyle.Bold);
                    }
                    listView_guarator_for.Items.Add(listViewItemss);
                }
            }

            //SELECT member_of -------------------------------------
            //-------------------------------------------------------------------------
            query = @"EXEC ('USE ' + @database_select + '
            SELECT g.id AS id,
            g.name AS [client_name],
            ''SolidarityGroup'' AS group_type,
                        (CASE WHEN t2.active=1 THEN ''Yes''
		                ELSE ''No'' END) AS [status],
            ISNULL(CONVERT(NVARCHAR(10), g.establishment_date, 120), ''-'') AS establishment_date,
            ISNULL(CONVERT(NVARCHAR(10), pgb.joined_date, 120), ''-'') AS joined_date,
            ISNULL(CONVERT(NVARCHAR(10), pgb.left_date, 120), ''-'') AS left_date
            FROM dbo.Tiers t
            LEFT JOIN dbo.Persons p ON p.id=t.id
            LEFT JOIN dbo.PersonGroupBelonging pgb ON pgb.person_id=p.id
            LEFT JOIN dbo.Groups g ON g.id=pgb.group_id
            LEFT JOIN dbo.Tiers t2 ON t2.id=g.id
            WHERE pgb.person_id='+@client_id+'

            UNION ALL

            SELECT v.id AS id,
            v.name AS [client_name],
            ''NonSolidarityGroup'' AS group_type,
                        (CASE WHEN t2.active=1 THEN ''Yes''
		                ELSE ''No'' END) AS [status],
            ISNULL(CONVERT(NVARCHAR(10), v.establishment_date, 120), ''-'') AS establishment_date,
            ISNULL(CONVERT(NVARCHAR(10), vp.joined_date, 120), ''-'') AS joined_date,
            ISNULL(CONVERT(NVARCHAR(10), vp.left_date, 120), ''-'') AS left_date
            FROM dbo.Tiers t
            LEFT JOIN dbo.Persons p ON p.id=t.id
            LEFT JOIN dbo.VillagesPersons vp ON vp.person_id=p.id
            LEFT JOIN dbo.Villages v ON v.id=vp.village_id
            LEFT JOIN dbo.Tiers t2 ON t2.id=v.id
            WHERE vp.person_id='+@client_id+'

            UNION ALL

            SELECT corp.id AS id,
            corp.name AS [client_name],
            ''Corporate'' AS group_type,
                        (CASE WHEN t.active=1 THEN ''Yes''
		                ELSE ''No'' END) AS [status],
            ''-'' AS establishment_date,
            ''-'' AS joined_date,
            ''-'' AS left_date
            FROM dbo.CorporatePersonBelonging cpb
            LEFT JOIN dbo.Persons p ON p.id=cpb.person_id
            LEFT JOIN dbo.Corporates corp ON corp.id=cpb.corporate_id
            LEFT JOIN dbo.Tiers t ON t.id=corp.id
            WHERE p.id='+@client_id+'')";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", SearchForm_v.list_selected_pid);
                cmd.Parameters.AddWithValue("@database_select", SearchForm_v.list_selected_dbname);
                var reader = cmd.ExecuteReader();

                variables.i_member_of = 0; //set 0, because global
                if (reader.HasRows) { variables.i_member_of = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("group_type")),
                            status,
                            reader.GetString(reader.GetOrdinal("establishment_date")),
                            reader.GetString(reader.GetOrdinal("joined_date")),
                            reader.GetString(reader.GetOrdinal("left_date"))
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Yes")
                    {
                        listViewItemss.Font = new Font(listView_member_of.Font, FontStyle.Bold);
                    }
                    listView_member_of.Items.Add(listViewItemss);
                }
            }

            //SELECT solidary_group_loan -------------------------------------
            //-------------------------------------------------------------------------
            query = @"EXEC ('USE ' + @database_select + '
                    SELECT c.id AS id,
                    g.name AS [client_name],
                    c.contract_code AS contract_code,
                    s.status_name AS status_name,
                    cr.amount AS amount,
                    lsa.amount AS lsa_amount,
                    CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
                    CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
                    u.last_name+SPACE(1)+u.first_name AS LO
                    FROM dbo.Contracts c
                    LEFT JOIN dbo.Credit cr ON cr.id=c.id
                    LEFT JOIN dbo.Statuses s ON s.id=c.status
                    LEFT JOIN dbo.Users u ON u.id=cr.loanofficer_id
                    LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
                    LEFT JOIN dbo.Persons p ON p.id=t.id
                    LEFT JOIN dbo.Groups g ON g.id=t.id
                    LEFT JOIN dbo.LoanShareAmounts lsa ON lsa.contract_id=c.id
                    WHERE lsa.person_id='+@client_id+'')";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", SearchForm_v.list_selected_pid);
                cmd.Parameters.AddWithValue("@database_select", SearchForm_v.list_selected_dbname);
                var reader = cmd.ExecuteReader();

                variables.i_solidary_group_loan = 0; //set 0, because global
                if (reader.HasRows) { variables.i_solidary_group_loan = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    Decimal lsa_amount = reader.GetDecimal(reader.GetOrdinal("lsa_amount"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            lsa_amount.ToString(),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            reader.GetString(reader.GetOrdinal("LO")),
                            //reader.GetString(reader.GetOrdinal("col_prod"))
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(listView_solidary_group_loan.Font, FontStyle.Bold);
                    }
                    listView_solidary_group_loan.Items.Add(listViewItemss);
                }
            }

            //SELECT collateral_owner -------------------------------------
            //-------------------------------------------------------------------------
            query = @"EXEC ('USE ' + @database_select + '
                    SELECT c.id AS id,
                    ISNULL(g.name, p.first_name + SPACE(1) + p.last_name+ SPACE(1)+ISNULL(p.father_name, '''')) AS [client_name],
                    c.contract_code AS contract_code,
                    s.status_name AS status_name,
                    cr.amount AS amount,
                    CONVERT(NVARCHAR(10), c.[start_date], 120) AS [start_date],
                    CONVERT(NVARCHAR(10), c.close_date, 120) AS close_date,
                    u.last_name+SPACE(1)+u.first_name AS LO,
                    col_prod.name AS col_prod
                    FROM CollateralPropertyValues cpv
                    LEFT JOIN dbo.CollateralProperties cp ON cp.id=cpv.property_id
                    LEFT JOIN dbo.CollateralProducts col_prod ON col_prod.id=cp.product_id
                    LEFT JOIN dbo.CollateralPropertyTypes cpt ON cpt.id=cp.type_id
                    LEFT JOIN dbo.CollateralsLinkContracts clc ON clc.id=cpv.contract_collateral_id
                    LEFT JOIN dbo.Contracts c ON c.id=clc.contract_id
                    LEFT JOIN dbo.Credit cr ON cr.id=c.id
                    LEFT JOIN dbo.Statuses s ON s.id=c.status
                    LEFT JOIN dbo.Users u ON u.id=cr.loanofficer_id
                    LEFT JOIN dbo.Tiers t ON t.id=c.tiers_id
                    LEFT JOIN dbo.Persons p ON p.id=t.id
                    LEFT JOIN dbo.Groups g ON g.id=t.id
                    WHERE cpv.value=CAST('+@client_id+' AS NVARCHAR(10)) AND cpt.name=''Owner''')";

            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@client_id", SearchForm_v.list_selected_pid);
                cmd.Parameters.AddWithValue("@database_select", SearchForm_v.list_selected_dbname);
                var reader = cmd.ExecuteReader();

                variables.i_collateral_owner = 0; //set 0, because global
                if (reader.HasRows) { variables.i_collateral_owner = 1; } //defining rows for hide or close this part

                while (reader.Read())
                {
                    Decimal id = reader.GetInt32(reader.GetOrdinal("id"));
                    String status = reader.GetString(reader.GetOrdinal("status_name"));
                    Decimal amount = reader.GetDecimal(reader.GetOrdinal("amount"));
                    string[] row = {
                            id.ToString(),
                            reader.GetString(reader.GetOrdinal("client_name")),
                            reader.GetString(reader.GetOrdinal("contract_code")),
                            status,
                            amount.ToString(),
                            reader.GetString(reader.GetOrdinal("start_date")),
                            reader.GetString(reader.GetOrdinal("close_date")),
                            reader.GetString(reader.GetOrdinal("LO")),
                            reader.GetString(reader.GetOrdinal("col_prod"))
                            };
                    var listViewItemss = new ListViewItem(row);
                    if (status == "Active")
                    {
                        listViewItemss.Font = new Font(listView_collateral_owner.Font, FontStyle.Bold);
                    }
                    listView_collateral_owner.Items.Add(listViewItemss);
                }
            }

//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
            if (variables.i_loans == 1)
            {
                link_loans.Text = lmax_loans;
                panel_loans.Visible = true;
                variables.i_loans = 0;
            }
            else
            {
                link_loans.Text = lmin_loans;
                panel_loans.Visible = false;
                variables.i_loans = 1;
            }
            //--
            if (variables.i_savings == 1)
            {
                link_savings.Text = lmax_savings;
                panel_savings.Visible = false; //temprorary disabled saving
                variables.i_savings = 0;
            }
            else
            {
                link_savings.Text = lmin_savings;
                panel_savings.Visible = false; 
                variables.i_savings = 1;
            }
            //--


            if (variables.i_guarantor_for == 1)
            {
                link_guarator_for.Text = lmax_guarantor_for;
                panel_guarantor_for.Visible = true;
                variables.i_guarantor_for = 0;
            }
            else
            {
                link_guarator_for.Text = lmin_guarantor_for;
                panel_guarantor_for.Visible = false;
                variables.i_guarantor_for = 1;
            }
            //--
            if (variables.i_member_of == 1)
            {
                link_member_of.Text = lmax_member_of;
                panel_member_of.Visible = true;
                variables.i_member_of = 0;
            }
            else
            {
                link_member_of.Text = lmin_member_of;
                panel_member_of.Visible = false;
                variables.i_member_of = 1;
            }
            //--
            if (variables.i_solidary_group_loan == 1)
            {
                link_solidary_group_loan.Text = lmax_solidary_group_loan;
                panel_solidary_group_loan.Visible = true;
                variables.i_solidary_group_loan = 0;
            }
            else
            {
                link_solidary_group_loan.Text = lmin_solidary_group_loan;
                panel_solidary_group_loan.Visible = false;
                variables.i_solidary_group_loan = 1;
            }
            //--
            if (variables.i_collateral_owner == 1)
            {
                link_collateral_owner.Text = lmax_collateral_owner;
                panel_collateral_owner.Visible = true;
                variables.i_collateral_owner = 0;
            }
            else
            {
                link_collateral_owner.Text = lmin_collateral_owner;
                panel_collateral_owner.Visible = false;
                variables.i_collateral_owner = 1;
            }
        }

//----------------------------------------------------------------------------------------------


        private void link_loans_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_loans == 1)
            {
                link_loans.Text = lmax_loans;
                panel_loans.Visible = true;
                variables.i_loans = 0;
            }
            else
            {
                link_loans.Text = lmin_loans;
                panel_loans.Visible = false;
                variables.i_loans = 1;
            }
        }

        private void link_savings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_savings == 1)
            {
                link_savings.Text = lmax_savings;
                panel_savings.Visible = true;
                variables.i_savings = 0;
            }
            else
            {
                link_savings.Text = lmin_savings;
                panel_savings.Visible = false;
                variables.i_savings = 1;
            }
        }

        private void link_guarator_for_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_guarantor_for == 1)
            {
                link_guarator_for.Text = lmax_guarantor_for;
                panel_guarantor_for.Visible = true;
                variables.i_guarantor_for = 0;
            }
            else
            {
                link_guarator_for.Text = lmin_guarantor_for;
                panel_guarantor_for.Visible = false;
                variables.i_guarantor_for = 1;
            }
        }

        private void link_member_of_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_member_of == 1)
            {
                link_member_of.Text = lmax_member_of;
                panel_member_of.Visible = true;
                variables.i_member_of = 0;
            }
            else
            {
                link_member_of.Text = lmin_member_of;
                panel_member_of.Visible = false;
                variables.i_member_of = 1;
            }
        }

        private void link_solidary_group_loan_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_solidary_group_loan == 1)
            {
                link_solidary_group_loan.Text = lmax_solidary_group_loan;
                panel_solidary_group_loan.Visible = true;
                variables.i_solidary_group_loan = 0;
            }
            else
            {
                link_solidary_group_loan.Text = lmin_solidary_group_loan;
                panel_solidary_group_loan.Visible = false;
                variables.i_solidary_group_loan = 1;
            }
        }

        private void link_collateral_owner_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (variables.i_collateral_owner == 1)
            {
                link_collateral_owner.Text = lmax_collateral_owner;
                panel_collateral_owner.Visible = true;
                variables.i_collateral_owner = 0;
            }
            else
            {
                link_collateral_owner.Text = lmin_collateral_owner;
                panel_collateral_owner.Visible = false;
                variables.i_collateral_owner = 1;
            }
        }

        private void label_clientname_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if (thisDatabase == 1)
            //{

            //    //Person p = (Person)ServicesProvider.GetInstance().GetClientServices().FindTiers(SearchForm_v.list_selected_pid, OClientTypes.Person); //FindPersonById(SearchForm_v.list_selected_pid);
            //    Person p = ServicesProvider.GetInstance().GetClientServices().FindPersonById(SearchForm_v.list_selected_pid);
            //    var mainForm = (OctopusMainForm)Application.OpenForms[0];
            //    mainForm.InitializePersonForm(p, null);
            //    Close();
            //    //var f = new ClientForm(p, this);
            //    //f.MdiParent = Application.OpenForms[0].ParentForm; //this refers to f's parent, the MainForm
            //    //f.Show();

            //}
            //else
            //{
            //    MessageBox.Show("You can not open, because this object in another database");
            //}
        }

        private void listView_loans_DoubleClick(object sender, EventArgs e)
        {
            //if (thisDatabase == 1)
            //{
            //    int contract_id = int.Parse(listView_loans.SelectedItems[0].SubItems[0].Text);
            //    IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            //    var mainForm = (OctopusMainForm)Application.OpenForms[0];
            //    mainForm.InitializeCreditContractForm(client, contract_id);
            //    Close();
            //}
            //else
            //{
            //    MessageBox.Show("You can not open, because this object in another database");
            //}
        }

        private void listView_guarator_for_DoubleClick(object sender, EventArgs e)
        {
            //if (thisDatabase == 1)
            //{
            //    int contract_id = int.Parse(listView_guarator_for.SelectedItems[0].SubItems[0].Text);
            //    IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            //    var mainForm = (OctopusMainForm)Application.OpenForms[0];
            //    mainForm.InitializeCreditContractForm(client, contract_id);
            //    Close();
            //}
            //else
            //{
            //    MessageBox.Show("You can not open, because this object in another database");
            //}
        }
        

        private void listView_member_of_DoubleClick(object sender, EventArgs e)
        {
            //if (thisDatabase == 1)
            //{
            //    if ((listView_member_of.SelectedItems[0].SubItems[2].Text) == "SolidarityGroup")
            //    {
            //        int group_id = int.Parse(listView_member_of.SelectedItems[0].SubItems[0].Text);
            //        Group g = ServicesProvider.GetInstance().GetClientServices().FindGroupById(group_id);
            //        var f = new ClientForm(g, ClientForm.ActiveForm);
            //        f.ShowDialog();
            //    }
            //    else if ((listView_member_of.SelectedItems[0].SubItems[2].Text) == "NonSolidarityGroup")
            //    {
            //        int nsgroup_id = int.Parse(listView_member_of.SelectedItems[0].SubItems[0].Text);
            //        Village g = (Village)ServicesProvider.GetInstance().GetClientServices().FindTiers(nsgroup_id, OClientTypes.Village);
            //        var f = new NonSolidaryGroupForm(g);
            //        //frm.Show();
            //        //var mainForm = (LotrasmicMainWindowForm) Application.OpenForms[0];
            //        //mainForm.InitializeVillageForm((g));
            //        f.ShowDialog();
            //    }
            //    else if ((listView_member_of.SelectedItems[0].SubItems[2].Text) == "Corporate")
            //    {
            //        int corp_id = int.Parse(listView_member_of.SelectedItems[0].SubItems[0].Text);
            //        Corporate g = (Corporate)ServicesProvider.GetInstance().GetClientServices().FindTiers(corp_id, OClientTypes.Corporate);
            //        var f = new ClientForm(g, ClientForm.ActiveForm);
            //        f.ShowDialog();
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("You can not open, because this object in another database");
            //}
        }
        

        private void listView_solidary_group_loan_DoubleClick(object sender, EventArgs e)
        {
            //if (thisDatabase == 1)
            //{
            //    int contract_id = int.Parse(listView_solidary_group_loan.SelectedItems[0].SubItems[0].Text);
            //    IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            //    var mainForm = (OctopusMainForm)Application.OpenForms[0];
            //    mainForm.InitializeCreditContractForm(client, contract_id);
            //    Close();
            //}
            //else
            //{
            //    MessageBox.Show("You can not open, because this object in another database");
            //}
        }
        

        private void listView_collateral_owner_DoubleClick(object sender, EventArgs e)
        {
            //if (thisDatabase == 1)
            //{
            //    int contract_id = int.Parse(listView_collateral_owner.SelectedItems[0].SubItems[0].Text);
            //    IClient client = ServicesProvider.GetInstance().GetClientServices().FindTiersByContractId(contract_id);
            //    var mainForm = (OctopusMainForm)Application.OpenForms[0];
            //    mainForm.InitializeCreditContractForm(client, contract_id);
            //    Close();
            //}
            //else
            //{
            //    MessageBox.Show("You can not open, because this object in another database");
            //}
        }

        private void listView_loans_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        


    }
}
