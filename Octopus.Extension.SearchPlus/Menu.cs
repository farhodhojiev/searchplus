﻿using System;
using System.Windows.Forms;
using Octopus.Extensions;

namespace Octopus.Extension.SearchPlus
{
    class Menu : IMenu
    {
        public ExtensionMenuItem[] GetItems()
        {
            var item = new ToolStripMenuItem
            {
                Name = "mnuSearchPlus",
                Text = "SearchPlus",
            };

            item.Click += OnClick;
            //item.Click += (o, e) =>
            //{
            //    var frm = new SearchForm();
            //    frm.Show();
            //};

            var extensionItem = new ExtensionMenuItem
            {
                InsertAfter = "miSearchClient",
                MenuItem = item
            };
            return new[] { extensionItem };
        }

        private static void OnClick(object sender, EventArgs e)
        {
            var frm = new SearchForm { MdiParent = Application.OpenForms[0] };
            frm.Show();
        }
    }
}
