﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Octopus.CoreDomain;
using System.Data.SqlClient;

namespace Octopus.Extension.SearchPlus
{
    public partial class SearchForm : Form
    {
        public int list_selected_pid; // need to send selected items from list
        public string list_selected_dbname; //need to send selected items from list

        public class ComboboxItem //this class for index of CheckListBox
        {
            public string Text { get; set; }
            public object Value { get; set; }
            //public bool Enabled { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }


        public SearchForm()
        {
            InitializeComponent();
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            //////////////////////////////////////////////////// load list databases
            string query = @"DECLARE @t TABLE (id INT, DBName sysname not null)
                    INSERT INTO @t
                    EXEC sp_msforeachdb 'use [?]; if OBJECT_ID(''dbo.TechnicalParameters'') is not null select db_id(), db_name()'
                    SELECT id, DBName FROM @t";
            using (var conn = DatabaseConnection.GetConnection())
            {
                var cmd = new SqlCommand(query, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = reader.GetString(reader.GetOrdinal("DBName"));
                    item.Value = reader.GetInt32(reader.GetOrdinal("id"));
                    checkedListBoxDatabases.Items.Add(item, true);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (checkedListBoxDatabases.CheckedItems.Count != 0) //check minimum databasae choosed
            {
                //////////////////////////////////////////////////// Select list of database values
                string s = "";
                foreach (var value in checkedListBoxDatabases.CheckedItems)
                {
                    s = s + ((value as ComboboxItem).Value.ToString()) + ",";
                }
                s = s.Substring(0, s.Length - 1);
                //////////////////////////////////////////////////// Search Contracts
                listView_all.Items.Clear();
                string query = @"DECLARE @all_cli TABLE (
	                id INT,
	                s_basename NVARCHAR(100),
	                FIO NVARCHAR(200),
	                Passport NVARCHAR(20),
	                [status] NVARCHAR(20),
                    loan_cycle INT,
	                bad_client NVARCHAR(20),
	                Branch NVARCHAR(100),
	                District NVARCHAR(100),
	                custom_fields NVARCHAR(MAX)
                )


                --------------------------------------------------------------------------------
                --------------------------------------------------------------------------------
	                DECLARE @Bases TABLE (name NVARCHAR(100))
	                DECLARE @BaseName NVARCHAR(100)
	                DECLARE @sql VARCHAR(1000)
	                SELECT @sql='SELECT name FROM master.dbo.sysdatabases WHERE dbid IN ('+@database_select+')'
	                INSERT INTO @Bases
	                EXEC(@sql)
                	
		                DECLARE TableCursor CURSOR FOR 
		                SELECT name FROM @Bases
		                OPEN TableCursor

		                FETCH NEXT FROM TableCursor INTO @BaseName 
		                WHILE @@FETCH_STATUS = 0 
		                BEGIN 
                --------------------------------------------------------------------------------
                				
		                INSERT INTO @all_cli
		                EXEC ('USE ' + @BaseName + '
		                SELECT TOP 100 
		                p.id,
		                ''' + @BaseName + ''' as s_basename,
		                p.first_name+SPACE(1)+p.last_name+SPACE(1)+ISNULL(p.father_name,'''') AS FIO,
		                p.identification_data,
		                (CASE 
		                WHEN t.active=1 THEN ''Yes''
		                ELSE ''No'' END) AS [status],
		                --(CASE 
		                --WHEN t.bad_client=1 THEN ''Yes''
		                --ELSE ''No'' END) AS bad_client,
                        t.loan_cycle AS loan_cycle,
                        ''No'' AS bad_client,
		                b.name AS Branch,
		                d.name AS District,
		                ISNULL(cfv.value, ''-'') AS custom_fields

		                FROM dbo.Persons p
		                LEFT JOIN dbo.Tiers t ON p.id=t.id
		                LEFT JOIN dbo.Branches b ON b.id=t.branch_id
		                LEFT JOIN dbo.Districts d ON d.id=t.district_id
                        LEFT JOIN dbo.CustomFields cf ON cf.guid=''INN''
		                LEFT JOIN dbo.CustomFieldsValues cfv ON cfv.owner_id=p.id AND cfv.field_id=cf.id
		                WHERE p.identification_data LIKE ''%'+@search_text+'%''
		                OR p.last_name+SPACE(1)+p.first_name+SPACE(1)+ISNULL(p.father_name,'''') LIKE ''%'+@search_text+'%''
		                OR p.first_name+SPACE(1)+p.last_name+SPACE(1)+ISNULL(p.father_name,'''') LIKE ''%'+@search_text+'%''
		                OR cfv.value LIKE ''%'+@search_text+'%''
		                ')
                --------------------------------------------------------------------------------		
		                FETCH NEXT FROM TableCursor INTO @BaseName 
		                END 
		                CLOSE TableCursor  
		                DEALLOCATE TableCursor
                --------------------------------------------------------------------------------
                --------------------------------------------------------------------------------
		                SELECT * FROM @all_cli";
                using (var conn = DatabaseConnection.GetConnection())
                {
                    var cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@database_select", s);
                    cmd.Parameters.AddWithValue("@search_text", SearchClientBox.Text);
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        String bad_client = reader.GetString(reader.GetOrdinal("bad_client"));
                        String client_status = reader.GetString(reader.GetOrdinal("status"));
                        Int32 idp = reader.GetInt32(reader.GetOrdinal("id"));
                        Int32 loan_cycle = reader.GetInt32(reader.GetOrdinal("loan_cycle"));
                        string[] row = {
                            idp.ToString(),
                            reader.GetString(reader.GetOrdinal("s_basename")),
                            reader.GetString(reader.GetOrdinal("FIO")),
                            reader.GetString(reader.GetOrdinal("Passport")),
                            client_status,
                            loan_cycle.ToString(),
                            bad_client,
                            reader.GetString(reader.GetOrdinal("Branch")),
                            reader.GetString(reader.GetOrdinal("District")),
                            reader.GetString(reader.GetOrdinal("custom_fields")),
                            };
                        var listViewItemss = new ListViewItem(row);
                        if (client_status == "Yes")
                        {
                            listViewItemss.Font = new Font(listView_all.Font, FontStyle.Bold);
                        }
                        if (bad_client == "Yes")
                        {
                            System.Drawing.Color col = Color.Red;
                            listViewItemss.BackColor = col;
                        }
                        listView_all.Items.Add(listViewItemss);
                    }
                }
            }else{
                MessageBox.Show("Please choose any database");
            }
        }

        private void listView_all_DoubleClick(object sender, EventArgs e)
        {
            var frm_info = new InfoForm();
            list_selected_pid = int.Parse(listView_all.SelectedItems[0].SubItems[0].Text);
            list_selected_dbname = listView_all.SelectedItems[0].SubItems[1].Text;
            frm_info.SearchForm_v = this; //for connection with other forms
            frm_info.Show();
        }

        private void listView_all_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
